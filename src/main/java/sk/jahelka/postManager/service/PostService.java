package sk.jahelka.postManager.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.jahelka.postManager.exception.PostNotFoundException;
import sk.jahelka.postManager.exception.UserNotFoundException;
import sk.jahelka.postManager.model.Post;
import sk.jahelka.postManager.model.User;
import sk.jahelka.postManager.repository.PostRepository;
import sk.jahelka.postManager.repository.UserRepository;

@Service
public class PostService {
	
	
	
	private final PostRepository postRepository;
	private final UserRepository userRepository;

    @Autowired
    public PostService(PostRepository postRepository, UserRepository userRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
    }
    
    public List<Post> getAllPosts() {
        return postRepository.findAll();
    }
    
    public Optional<Post> getPostById(Long postId) {
        return postRepository.findById(postId);
    }

    public List<Post> getPostsByUserId(Long userId) {
        return postRepository.findByUserId(userId);
    }

    public Post addPost(Post post) {
        return postRepository.save(post);
    }

    public void deletePost(Long postId) {
        postRepository.deleteById(postId);
    }

    public Post updatePost(Long postId, Post updatedPost) {
        // validácia existencie príspevku
        Post existingPost = postRepository.findById(postId).orElseThrow(() -> new PostNotFoundException(postId));

        // prípadná validácia a aktualizácia príspevku
        existingPost.setTitle(updatedPost.getTitle());
        existingPost.setBody(updatedPost.getBody());

        return postRepository.save(existingPost);
    }
}
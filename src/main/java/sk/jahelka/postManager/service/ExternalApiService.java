package sk.jahelka.postManager.service;

import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import sk.jahelka.postManager.model.Post;
import sk.jahelka.postManager.model.User;

@Service
public class ExternalApiService {
	
	  private final String EXTERNAL_API_URL = "https://jsonplaceholder.typicode.com/";

	    private final RestTemplate restTemplate;

	    public ExternalApiService(RestTemplate restTemplate) {
	        this.restTemplate = restTemplate;
	    }

	    //ex. "https://jsonplaceholder.typicode.com/users/1"
	    public Optional<User> fetchUserFromExternalApi(Long id) {
	    	
	        String url = EXTERNAL_API_URL + "/users/" + id;
	        return Optional.ofNullable(restTemplate.getForObject(url, User.class));  
	    }
	    
	    //ex. "https://jsonplaceholder.typicode.com/posts/1"
	    public Optional<Post> fetchPostFromExternalApi(Long id) {
	    	
	        String url = EXTERNAL_API_URL + "/posts/" + id;
	        return Optional.ofNullable(restTemplate.getForObject(url, Post.class));  
	    }
}
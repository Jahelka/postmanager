package sk.jahelka.postManager.data;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import sk.jahelka.postManager.model.Post;
import sk.jahelka.postManager.service.PostService;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Component
@DependsOn("postService")
public class DataInitializer implements CommandLineRunner {

    private final PostService postService;
    
    @Autowired
    public DataInitializer(PostService postService) {
        this.postService = postService;
    }

    @Override
    public void run(String... args) throws Exception {
    	
    	 // JSON file import
        String jsonFilePath = "src/main/resources/json/posts.json";

        // Jackson ObjectMapper to read JSON file
        ObjectMapper objectMapper = new ObjectMapper();
        List<Post> jsonPosts = objectMapper.readValue(new File(jsonFilePath), new TypeReference<List<Post>>() {});

        // manually create some sample data
        Post post1 = new Post(101L, 1L,  "Title 101", "Body 1");
        Post post2 = new Post(102L, 10L, "Title 102", "Body 10");
        List<Post> manualPosts = new ArrayList<>();
        manualPosts.add(post1);
        manualPosts.add(post2); 
        
        // Add posts to the database
//        for (Post post : jsonPosts) { // manualPosts
//            postService.addPost(post);
//        }


    }
}
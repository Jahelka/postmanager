package sk.jahelka.postManager.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import sk.jahelka.postManager.model.Post;
import sk.jahelka.postManager.model.User;
import sk.jahelka.postManager.service.ExternalApiService;
import sk.jahelka.postManager.service.PostService;
import sk.jahelka.postManager.exception.PostNotFoundException;
import sk.jahelka.postManager.exception.UserNotFoundException;

@RestController
@Validated
@RequestMapping("/api/posts")
@Api(tags = "Post Controller", description = "Operations related to managing posts")
public class PostController {

	private final PostService postService;
	private final ExternalApiService externalApiService;


    @Autowired
    public PostController(PostService postService, ExternalApiService externalApiService) {
        this.postService = postService;
        this.externalApiService = externalApiService;
    }
    
    
    @PostMapping
    @ApiOperation(value = "Add a new post", response = Post.class)
    public ResponseEntity<?> addPost(
    		@ApiParam(value = "Post details", required = true)
    		@RequestBody Post post) {
        
    	// the user must exist
    	Optional<User> userOptional = externalApiService.fetchUserFromExternalApi(post.getUserId());

        if (userOptional.isPresent()) {
            //User user = userOptional.get();
            
            // save post
            Post addedPost = postService.addPost(post);
            return new ResponseEntity<>(addedPost, HttpStatus.CREATED);
        } else {
        	throw new UserNotFoundException(post.getUserId());
        }
    }
    
    @GetMapping("/{id}")
    @ApiOperation(value = "Get a post by post ID", response = Post.class)
    public ResponseEntity<?> getPostById(
    		@ApiParam(value = "ID of the post", example = "1", required = true)
    		@PathVariable Long id) {
    	
    	// local post
    	Optional<Post> localPostOptional = postService.getPostById(id);
    	if(localPostOptional.isPresent()) {
    		return ResponseEntity.ok(localPostOptional);    
    	} else {
    		try {
    		//external post
    		Optional<Post> postOptional = externalApiService.fetchPostFromExternalApi(id);
        	
        	if (postOptional.isPresent()) {
                //Post found in the external API
                Post post = postOptional.get();
                postService.addPost(post);
                return ResponseEntity.ok(post);    
            } else {
            	// post is neither in local nor external
            	throw new PostNotFoundException(id);
            }
    		 }
    		
    		catch (HttpClientErrorException.NotFound e) {
                // Handle 404 error from external API
                throw new PostNotFoundException(id);
            }
        }
    }
    
    @GetMapping("/user/{userId}")
    @ApiOperation(value = "Get a posts by user ID", response = List.class)
    public List<Post> getPostsByUserId(
    		@ApiParam(value = "ID of user", example = "1", required = true)
    		@PathVariable Long userId) {
        return postService.getPostsByUserId(userId);
    }
    
    @GetMapping
    @ApiOperation(value = "Get all posts in local", response = List.class)
    public List<Post> getAllPosts() {
        return postService.getAllPosts();
    }

    @PutMapping("/{postId}")
    @ApiOperation(value = "Update a post by post ID", response = Post.class)
    public ResponseEntity<Post> updatePost(
    		@ApiParam(value = "Updated post details", required = true)
    		@PathVariable Long postId, 
    		@RequestBody Post updatedPost) {
    	 try {
             Post updated = postService.updatePost(postId, updatedPost);
             return ResponseEntity.ok(updated);
         } catch (PostNotFoundException e) {
             return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
         }	
    }

    @DeleteMapping("/{postId}")
    @ApiOperation(value = "Delete a post by ID")
    public ResponseEntity<Void> deletePost(
    		@ApiParam(value = "ID of the post", example = "1", required = true)
    		@PathVariable Long postId) {
        postService.deletePost(postId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
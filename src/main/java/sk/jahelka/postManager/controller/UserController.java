package sk.jahelka.postManager.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Validated
@RequestMapping("/api/users")
public class UserController {

    @GetMapping("/validate/{userId}")
    public boolean isValidUserId(@PathVariable Long userId) {
        // Call external API or perform validation logic here
        // For simplicity, assume it always returns true
        return true;
    }
}
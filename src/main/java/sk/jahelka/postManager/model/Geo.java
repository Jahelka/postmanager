package sk.jahelka.postManager.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "geo")
@XmlAccessorType(XmlAccessType.FIELD)
public class Geo {
	
	@JsonProperty("lat")
    private String lat;
    
	@JsonProperty("lng")
    private String lng;

    public Geo() {
    }

    public Geo(String lat, String lng) {
		super();
		this.lat = lat;
		this.lng = lng;
	}

	@JsonProperty("lat")
    public String getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(String lat) {
        this.lat = lat;
    }

    @JsonProperty("lng")
    public String getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public void setLng(String lng) {
        this.lng = lng;
    }
}
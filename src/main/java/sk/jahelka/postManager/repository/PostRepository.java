package sk.jahelka.postManager.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import sk.jahelka.postManager.model.Post;

public interface PostRepository extends JpaRepository<Post, Long> {
    List<Post> findByUserId(Long userId);
}
package sk.jahelka.postManager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sk.jahelka.postManager.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    // prípadné metódy pre získanie používateľa podľa mena alebo iného kritéria
}
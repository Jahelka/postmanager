DROP ALL OBJECTS DELETE FILES;

CREATE SCHEMA IF NOT EXISTS postmanager;
SET SCHEMA postmanager;

-- Create Table

-- Posts
CREATE TABLE IF NOT EXISTS Posts (
    id BIGINT,
    userId BIGINT,
    title VARCHAR(255),
    body VARCHAR(1000)
);

Swagger documentation:

# PostManager Application

PostManager is a Spring Boot application that provides RESTful API for managing user posts. 
It allows you to add, retrieve, update, and delete posts, with validation and integration with an external user API.

## Installation

1. Clone the repository:

bash:
git clone https://gitlab.com/Jahelka/postmanager
cd postmanager
   
Build the application using Maven:

bash:
mvn clean install

Run the application:

bash:
java -jar target/postmanager-1.0.0.jar

The application will start on http://localhost:8080

API Documentation (Swagger)
To Swagger UI by navigating to: 
http://localhost:8080/swagger-ui/index.html

note: if swagger not working try to stop app remove and past back swagger dependency, start app..

